# decision_making_pacbot

Decision Making Repo
>This repo is a fortk of this [repo](https://gricad-gitlab.univ-grenoble-alpes.fr/pacbot_group/decision_making_pacbot)
## Getting started

1. Run Ros Master
```sh
$ roscore
```

2. Launch Planning Server
```sh
$ . devel/setup.bash
$ roslaunch ros_planning ros_planning.launch
```
