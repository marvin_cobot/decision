;-----------------------
;   Lego domain (3D)   |
;-----------------------
(define
    (domain lego)
    ;-------------------
    ;   Requirements   |
    ;-------------------
    (:requirements 
        :strips ; add delete effects.
        :typing ; add types.
        :negative-preconditions ; add negative preconditions.
        :action-costs ; add costs for the action.
        :disjunctive-preconditions
    )
    ;----------------
    ;   Types       |
    ;----------------
    (:types
        red_cube     - cube
        blue_cube    - cube
        yellow_cube  - cube
        light_cube   - cube
        olive_cube   - cube
        white_cube   - cube
        red_brick    - brick
        blue_brick   - brick
        yellow_brick - brick
        cube         - lego
        brick        - lego
        lego         - object
        robot        - agent
        human        - agent 
        agent        - object
        pos          - object
    )
    ;-------------------------------------------------------------------------
    ;                          Predicates                                    |
    ;-------------------------------------------------------------------------
    (:predicates
        (in_stock ?x - lego ?a - agent)
        (cube_at  ?c - cube  ?p - pos)
        (brick_at ?b - brick ?p1 ?p2 - pos)
        (empty ?p - pos)
        (under ?p1 ?p2 - pos)
    )
    ;------------------------------------------------------------------------
    ;                          Functions                                    |
    ;------------------------------------------------------------------------
    (:functions (total-cost))
    ;------------------------------------------------------------------------
    ;                          Actions                                      |
    ;------------------------------------------------------------------------
    ;;
    (:action pick_place_cube_human
    :parameters (?h - human ?c - cube ?p ?p0 - pos)
    :precondition 
        (and
            (in_stock ?c  ?h)
            (empty ?p)
            (under ?p0 ?p)
            (not (empty ?p0))
        )
    :effect
        (and
            (not (in_stock ?c  ?h))
            (cube_at  ?c ?p)
            (not (empty ?p))
            (increase (total-cost) 2)
        )
    )
    ;;
    (:action pick_place_brick_human
    :parameters (?h - human ?b - brick ?p1 ?p2 ?p01 ?p02 - pos)
    :precondition 
        (and
            (in_stock ?b  ?h)
            (empty ?p1)
            (empty ?p2)
            (under ?p01 ?p1)
            (under ?p02 ?p2)
            (not (empty ?p01))
            (not (empty ?p02))
        )
    :effect
        (and
            (not (in_stock ?b  ?h))
            (brick_at  ?b ?p1 ?p2)
            (not (empty ?p1))
            (not (empty ?p2))
            (increase (total-cost) 2)
        )
    )
    ;;
    (:action pick_place_cube_robot
    :parameters (?r - robot ?c - cube ?p ?p0 - pos)
    :precondition 
        (and
            (in_stock ?c  ?r)
            (empty ?p)
            (under ?p0 ?p)
            (not (empty ?p0))
        )
    :effect
        (and
            (not (in_stock ?c  ?r))
            (cube_at  ?c ?p)
            (not (empty ?p))
            (increase (total-cost) 1)
        )
    )
    ;;
    (:action pick_place_brick_robot
    :parameters (?r - robot ?b - brick ?p1 ?p2 ?p01 ?p02 - pos)
    :precondition 
        (and
            (in_stock ?b  ?r)
            (empty ?p1)
            (empty ?p2)
            (under ?p01 ?p1)
            (under ?p02 ?p2)
            (not (empty ?p01))
            (not (empty ?p02))
        )
    :effect
        (and
            (not (in_stock ?b  ?r))
            (brick_at  ?b ?p1 ?p2)
            (not (empty ?p1))
            (not (empty ?p2))
            (increase (total-cost) 1)
        )
    )
)