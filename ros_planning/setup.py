#!/usr/bin/python3

from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

# fetch values from package.xml
setup_args = generate_distutils_setup(
    scripts=[ 
    'src/ros_planning/ros_planning_node.py', 
    'src/ros_planning/problem_generator.py', 
    'src/ros_planning/pddl_problem_iface.py'
    ],
    packages=['ros_planning'],
    package_dir={'': 'src'},
    requires=['rospy', 'std_msgs']
)

setup(**setup_args)
