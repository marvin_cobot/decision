#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
| author:
| Belal HMEDAN, 
| LIG lab/ Marvin Team, 
| France, 2023.
| PDDL Problem Interafce.
"""
import json
import rospy
from std_msgs.msg import Int16
from ros_planning.problem_generator import ProblemCreator
from ros_planning.srv import Plan, UpdatePlan, UpdatePlanResponse, Act, ActResponse

class PDDLIface:
    def __init__(self):
        """ """
        self.planning_level = 1
        self.pc_ = None
        self.executed_legos = []
        self.initROS()

    def updateState(self):
        """
        Function: updateState, to update the PDDL problem from the vision model.
        ---
        Parameters:
        @param: data, JSON string, vision model dictionary.
        ---
        @return: None
        """
        model = json.loads(self.model)
        """
        model = { "p_02_08": {"1": " b"}, "p_02_09": {"1": "w", "2": "r"}, "p_07_14": {}}
        """
        rospy.logwarn("--- Model ---\n")
        rospy.logwarn(model)
        self.old_state = model
        # Reset
        self.occupied_positions = []
        # Fill Non-Empty Positions
        for pos in model:
            if bool(model[pos]):
                for z in model[pos]:
                    pos_z = pos + "_" + str(int(z) - 1)
                    rospy.logwarn(
                        "z: {}, pos {} planning_level: {}".format(
                            int(z), pos_z, self.planning_level
                        )
                    )
                    self.occupied_positions.append(pos_z)
        rospy.logwarn("New Plan Request\n")

    def generateProblem(self):
        self.pc_.initProblem()
        for lego in self.executed_legos:
            rospy.logwarn("Removing Lego: {}".format(lego))
            self.pc_.removeBlock(lego)
        self.pc_.generatePDDLProblem()

    def planIfaceCB(self, req):
        """ """
        self.model = req.model
        self.pattern = req.pattern
        self.arm = req.arm
        self.planning_level = req.planning_level
        # Update Occupied Positions from Model
        self.updateState()
        # Problem Generation
        if(self.pc_ == None):
            self.pc_ = ProblemCreator(self.pattern, self.arm)
        self.pc_.planning_level = self.planning_level
        self.pc_.occupied_positions = self.occupied_positions
        self.generateProblem()
        # TODO: check if Adding Delay here is necessary to assure writing problem.pddl to disk.
        resp = self.update_plan()
        self.plan = resp.plan
        return UpdatePlanResponse(self.plan, True)

    def actIfaceCB(self, req):
        """ """
        self.executed_legos.append(req.lego)
        return ActResponse(True)
    
    def initROS(self):
        """ """
        rospy.wait_for_service("fd_planning_service")
        self.update_plan = rospy.ServiceProxy("fd_planning_service", Plan)
        rospy.init_node('plan_srv_node')
        rospy.Service('planning_service', UpdatePlan, self.planIfaceCB)
        rospy.Service('remove_action', Act, self.actIfaceCB)
        rospy.spin()
        
##===================================================================
if __name__ == "__main__":
    fd_iface_ = PDDLIface()