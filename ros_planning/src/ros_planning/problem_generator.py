#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
| author:
| Belal HMEDAN, 
| LIG lab/ Marvin Team, 
| France, 2023.
| PDDL Problem Creator.
"""
import json
import rospkg
from itertools import permutations
import os

class ProblemCreator:
    def __init__(self, pattern, arm):
        """ """
        rospack = rospkg.RosPack()
        self.path = rospack.get_path("ros_planning")
        self.problem_path = self.path + "/model/problem.pddl"
        self.plan_path = self.path + "/model/plan.txt"
        self.occupied_positions = []
        self.pattern = pattern
        self.arm = arm
        self.level_counter = 0
        self.planning_level = 1
        self.problem = None
        self.predicates_map = {
            "wc": "white_cube",
            "rc": "red_cube",
            "yc": "yellow_cube",
            "bc": "blue_cube",
            "lc": "light_cube",
            "oc": "olive_cube",
            "yb": "yellow_brick",
            "bb": "blue_brick",
            "rb": "red_brick",
        }

    def loadData(self):
        """ """
        with open(self.path + "/database/lego_stock.json", "r") as fp:
            self.pattern_dict = json.load(fp)[self.pattern][self.arm]

        # Note that levels key is of type string and not int
        with open(self.path + "/database/levels.json", "r") as fp2:
            self.all_levels = json.load(fp2)
            self.levels = self.all_levels[str(self.planning_level-1)]
            self.under_positions = []
            if(self.planning_level>1):
                self.under_levels = self.all_levels[str(self.planning_level-2)]
                for color_key in self.under_levels:
                    self.under_positions.extend(self.under_levels[color_key])

            else:
                self.under_levels = {}

        with open(self.path + "/database/stock.json", "r") as fp3:
            self.stock = json.load(fp3)[self.pattern][self.arm]

    def objectsGenerator(self):
        """
        Function: objectsGenerator, to generate the objects of the PDDL problem.
        ---
        Parameters:
        @param: None.
        ---
        @return: objects, string, objects in PDDL problem format.
        """
        ## type : objects
        static_objects = [
            {"human": ["operator"]},
            {"robot": ["robot_arm"]},
            {"pos": ["platform"]},
        ]

        ## Base
        objects = ["(:objects", "\n", ")\n"]

        ## Static Objects
        for dict in static_objects:
            for typ in dict:
                for obj in dict[typ]:
                    objects.insert(-1, "    " + obj + "\n")
                objects.insert(-1, "    - " + typ + "\n")

        ## Dynamic Objects
        for lego_type in self.pattern_dict:
            ## Cube
            if lego_type[-4:] == "cube":
                ## Pos
                for pos in self.pattern_dict[lego_type][1]:
                    objects.insert(
                        objects.index("    - pos\n") - 1, "    " + pos + "\n"
                    )
                ## Cube
                for cub in self.pattern_dict[lego_type][0]:
                    objects.insert(-1, "    " + cub + "\n")
                objects.insert(-1, "    - " + lego_type + "\n")

            ## Brick
            elif lego_type[-5:] == "brick":
                ## Pos
                for brick in self.pattern_dict[lego_type][1]:
                    for pos in brick:
                        objects.insert(
                            objects.index("    - pos\n") - 1, "    " + pos + "\n"
                        )
                ## Brick
                for brk in self.pattern_dict[lego_type][0]:
                    objects.insert(-1, "    " + brk + "\n")
                objects.insert(-1, "    - " + lego_type + "\n")
        ## Under Pos
        for under_pos in self.under_positions:
            objects.insert(
                objects.index("    - pos\n") - 1, "    " + under_pos + "\n"
            )

        objects = " ".join(objects)
        return objects

    def findUnderPos(self, pos):
        """
        Function: findUnderPos, to find the position under the given one.
        ---
        Parameters:
        @param: pos, string, given position, p_xx_yy_z.
        ---
        @return: under_pos, string, position under given one, p_xx_yy_z.
        """
        new_pos = pos[:-1]
        under_level = str(int(pos[-1]) - 1)
        under_pos = new_pos + under_level
        return under_pos

    def initialStateGenerator(self):
        """
        Function: initialStateGenerator, to generate the initial state of the PDDL problem.
        ---
        Parameters:
        @param: None.
        ---
        @return: initial_state, string, initial_state in PDDL problem format.
        """
        ## Base
        initial_state = ["(:init", "\n", ")\n"]

        for color_index in self.pattern_dict:
            ## Cube
            if color_index[-4:] == "cube":
                ## Pos
                for pos in self.pattern_dict[color_index][1]:
                    if not pos in self.occupied_positions:
                        initial_state.insert(-1, "    ( empty {} )".format(pos) + "\n")
                    if pos[-1] == "0":
                        initial_state.insert(
                            -1, "    ( under platform {} )".format(pos) + "\n"
                        )
                    else:
                        initial_state.insert(
                            -1,
                            "    ( under {} {} )".format(self.findUnderPos(pos), pos)
                            + "\n",
                        )
                ## Cube
                for cub in self.pattern_dict[color_index][0]:
                    for agent in self.stock:
                        if cub in self.stock[agent]:
                            initial_state.insert(
                                -1, "    ( in_stock {} {} )".format(cub, agent) + "\n"
                            )

            ## Brick
            elif color_index[-5:] == "brick":
                ## Pos
                for brick in self.pattern_dict[color_index][1]:
                    for pos in brick:
                        if not pos in self.occupied_positions:
                            initial_state.insert(
                                -1, "    ( empty {} )".format(pos) + "\n"
                            )
                        if pos[-1] == "0":
                            initial_state.insert(
                                -1, "    ( under platform {} )".format(pos) + "\n"
                            )
                        else:
                            initial_state.insert(
                                -1,
                                "    ( under {} {} )".format(
                                    self.findUnderPos(pos), pos
                                )
                                + "\n",
                            )
                ## Brick
                for brk in self.pattern_dict[color_index][0]:
                    for agent in self.stock:
                        if brk in self.stock[agent]:
                            initial_state.insert(
                                -1, "    ( in_stock {} {} )".format(brk, agent) + "\n"
                            )

        initial_state = " ".join(initial_state)
        return initial_state

    def orBlockGenerator(self, blocks, positions, typ="cube"):
        """
        Function: orBlockGenerator, to generate the AND/OR disjunctions of the PDDL problem.
        ---
        Parameters:
        @param: blocks, list of strings, ctn: color/type/id number
        @param: positions, list of strings, p_xx_yy_z.
        @param: type, string, cube/brick.
        ---
        @return: or_block, string, AND/OR disjunctions in PDDL problem format.
        """
        ## Base
        or_block = ["    (or\n", "\n", "    )\n"]

        if typ == "cube":
            for blk_set in permutations(blocks, len(positions)):
                and_block = ["        (and\n", "\n", "            )\n"]
                for idx, pos in enumerate(positions):
                    if not pos in self.occupied_positions:
                        cube_at = "                ( cube_at {} {} )\n".format(blk_set[idx], pos)
                        if(not cube_at in and_block):
                            and_block.insert(-1, cube_at)
                ## Check And Block
                if(len(and_block) == 3):
                    and_block = ""
                elif (len(and_block) == 4):
                    and_block = and_block[2]
                else:
                    and_block = " ".join(and_block)
                space_and = "    " + and_block
                if not space_and in or_block:
                    or_block.insert(-1, "    " + and_block)

        elif typ == "brick":
            for blk_set in permutations(blocks, len(positions)):
                and_block = ["        (and\n", "\n", "            )\n"]
                for idx, pos in enumerate(positions):
                    if((not (pos[0] in self.occupied_positions)) and (not (pos[1] in self.occupied_positions))):
                        brick_at = "                ( brick_at {} {} {} )\n".format(blk_set[idx], pos[0], pos[1])
                        if(not brick_at in and_block):
                            and_block.insert(-1, brick_at)
                ## Check And Block
                if(len(and_block) == 3):
                    and_block = ""
                elif (len(and_block) == 4):
                    and_block = and_block[2]
                else:
                    and_block = " ".join(and_block)
                space_and = "    " + and_block
                if not space_and in or_block:
                    or_block.insert(-1, "    " + and_block)
        or_block = " ".join(or_block)
        if(or_block.count('b')>0):
            return or_block
        return ""

    def goalGenerator(self):
        """
        Function: goalGenerator, to generate the goal state of the PDDL problem.
        ---
        Parameters:
        @param: None.
        ---
        @return: goal_state, string, goal_state in PDDL problem format.
        """
        ## Base
        goal_state = ["(:goal\n (and", "\n", ")\n)"]

        for color_index in self.pattern_dict:
            if len(self.pattern_dict[color_index][1]) > 0:
                ## Cube
                if color_index[-4:] == "cube":
                    if len(self.pattern_dict[color_index][0]) == 1:
                        if not self.pattern_dict[color_index][1][0] in self.occupied_positions:
                            goal_state.insert(
                                -1,
                                "    ( cube_at {} {} )".format(
                                    self.pattern_dict[color_index][0][0],
                                    self.pattern_dict[color_index][1][0],
                                )
                                + "\n",
                            )
                    else:
                        ## permutations
                        goal_state.insert(
                            -1,
                            "    "
                            + self.orBlockGenerator(
                                self.pattern_dict[color_index][0],
                                self.pattern_dict[color_index][1],
                            ),
                        )

                ## Brick
                elif color_index[-5:] == "brick":
                    if len(self.pattern_dict[color_index][0]) == 1:
                        if((not (self.pattern_dict[color_index][1][0][0] in self.occupied_positions)) and (not (self.pattern_dict[color_index][1][0][1] in self.occupied_positions))):
                            goal_state.insert(
                                -1,
                                "    ( brick_at {} {} {} )".format(
                                    self.pattern_dict[color_index][0][0],
                                    self.pattern_dict[color_index][1][0][0],
                                    self.pattern_dict[color_index][1][0][1],
                                )
                                + "\n",
                            )
                    else:
                        ## permutations
                        goal_state.insert(
                            -1,
                            "    "
                            + self.orBlockGenerator(
                                self.pattern_dict[color_index][0],
                                self.pattern_dict[color_index][1],
                                typ="brick",
                            ),
                        )

        goal_state = " ".join(goal_state)
        return goal_state

    def problemGenerator(self):
        """
        Function: problemGenerator, to generate the PDDL problem.
        ---
        Parameters:
        @param: None.
        ---
        @return: None. {problem, string, problem in PDDL problem format}.
        """
        self.problem = None
        ## Base
        if self.pattern == "complex":
            problem = ["(define (problem complex)", "\n", "(:domain lego)", "\n", ")"]
        elif self.pattern == "simple":
            problem = ["(define (problem simple)", "\n", "(:domain lego)", "\n", ")"]
        else:
            print("Please choose correct pattern, simple or complex")
            return
        problem.insert(-1, self.objectsGenerator())
        problem.insert(-1, self.initialStateGenerator())
        problem.insert(-1, self.goalGenerator())

        problem = " ".join(problem)
        self.problem = problem

    def removeBlock(self, block):
        """
        Function: removeBlock, to remove a block from the PDDL problem.
        ---
        Parameters:
        @param: block, string, eg. 'rc1', 'bb2', etc.
        ---
        @return: None.
        """
        try:
            # Cube
            if block[1] == "c":
                for color_index in self.pattern_dict:
                    if block in self.pattern_dict[color_index][0]:
                        self.pattern_dict[color_index][0].remove(block)
            # Brick
            elif block[1] == "b":
                for color_index in self.pattern_dict:
                    # if(color_index[-5:]=="brick"):
                    if block in self.pattern_dict[color_index][0]:
                        self.pattern_dict[color_index][0].remove(block)

        except Exception as e:
            print(e)

    def removePos(self, pos):
        """
        Function: removePos, to remove a position from the PDDL problem.
        ---
        Parameters:
        @param: pos. string, p_xx_yy_z, eg. p_02_08_0.
        ---
        @return: None.
        """
        try:
            for color_index in self.pattern_dict:
                if pos in self.pattern_dict[color_index][1]:
                    self.pattern_dict[color_index][1].remove(pos)

        except Exception as e:
            print(e)
    
    def addLevel(self):
        """
        Function: addLevel, to add a level of blocks to the PDDL problem.
        ---
        Parameters:
        @param: None.
        ---
        @return: None.
        """
        for lego_type in self.levels:
            if lego_type[-4:] == "cube":
                for pos in self.levels[lego_type]:
                    if not (pos in self.pattern_dict[lego_type][1]):
                        self.pattern_dict[lego_type][1].append(pos)
            else:
                if not (
                    self.levels[lego_type]
                    in self.pattern_dict[lego_type][1]
                ):
                    self.pattern_dict[lego_type][1].append(
                        self.levels[lego_type]
                    )

    def writePDDLProblem(self):
        """
        Function: writePDDLProblem, to write the PDDL problem to the given path.
        ---
        Parameters:
        @param: None.
        ---
        @return: None.
        """
        with open(self.problem_path, "w") as pddl_file:
            pddl_file.write(self.problem)

    def initProblem(self):
        """ """
        self.loadData()
        if(self.pattern=="complex"):
            if(self.planning_level<6):
                self.addLevel()
        else:
            if(self.planning_level<2):
                self.addLevel()
        # Delete Old Problem
        if os.path.isfile(self.problem_path):
            os.remove(self.problem_path)
        # Delete Old Plan 
        if os.path.isfile(self.plan_path):
            os.remove(self.plan_path)

    def generatePDDLProblem(self):
        """
        Function: generatePDDLProblem, to generate and write the PDDL problem to the given path.
        ---
        Parameters:
        @param: None.
        ---
        @return: None.
        """
        self.problemGenerator()
        self.writePDDLProblem()

##========================================
## Test
# pattern_ = 'complex'
# arm_ = 'left'

# pc_ = ProblemCreator(pattern_, arm_)
# pc_.initProblem()

# pc_.planning_level = 1
# pc_.occupied_positions = ['p_02_08_0']
# pc_.removeBlock('wc2')

# pc_.generatePDDLProblem()