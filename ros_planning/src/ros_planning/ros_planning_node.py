#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
| author:
| Belal HMEDAN, 
| LIG lab/ Marvin Team, 
| France, 2023.
| FastDownward Planner Interafce.
"""
import rospy
import rospkg
import json
import subprocess
import time
import os
from ros_planning.srv import Plan,PlanResponse

class FDROSIface:
    def __init__(self):
        """ """
        rospack = rospkg.RosPack()
        self.path = rospack.get_path('ros_planning')
        self.domain_path  = self.path + "/model/domain.pddl"
        self.problem_path = self.path + "/model/problem.pddl"
        self.planner_path = self.path + "/downward/fast-downward.py"
        self.plan_path    = self.path + "/model/plan.txt"
        self.initROS()

    def callPlanner(self, req):
        """
        Function: callPlanner, to call the Fast-Downward PDDL Planner.
        """
        self.tic = time.time()
        plan = []
        # if os.path.isfile(self.plan_path):
        #     os.remove(self.plan_path)
        """
        https://stackoverflow.com/a/75515712
        """
        search_options = "lama-first"
        cmd = [
            "python3",
            self.planner_path,
            "--alias",
            search_options,
            "--plan-file",
            self.plan_path,
            self.domain_path,
            self.problem_path,
        ]
        subprocess.run(cmd).check_returncode()
        if os.path.isfile(self.plan_path):
            with open(self.plan_path, encoding="utf-8") as f:
                plan_msg = f.read()
                for action in plan_msg.splitlines()[:-1]:
                    splitted_action = action.split()[1:-1]
                    plan.append(splitted_action)

        self.toc = time.time()
        rospy.logwarn("Planning_time is: {} seconds".format(round(self.toc - self.tic, 3)))
        self.tic = self.toc
        rospy.logwarn(plan)
        return PlanResponse(json.dumps(plan), True)
    
    def initROS(self):
        """ """
        rospy.init_node('fd_planner_srv_node')
        rospy.Service('fd_planning_service', Plan, self.callPlanner)
        rospy.spin()

##=======================================================
if __name__ == "__main__":
    fd_iface_ = FDROSIface()